const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');

const Album = require('../models/Album');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.artist) {
        Album.find({artist: req.query.artist})
            .then(album => res.send(album))
            .catch(() => res.sendStatus(500))
    } else {
        Album.find()
            .then(album => res.send(album))
            .catch(() => res.sendStatus(500))
    }


});

router.get('/:id', (req, res) => {
    Album.find({artist: req.params.id}).populate('artist')
        .then(album => res.send(album)
        )
        .catch(() => res.sendStatus(500))
});

module.exports = router;
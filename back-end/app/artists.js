const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const config = require('../config');

const Artist = require('../models/Artist');
const Album = require('../models/Album');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    Artist.find()
        .then(artists => res.send(artists))
        .catch(() => res.sendStatus(500))
});


router.get('/:id', (req, res) => {
    Album.find({artist: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.send(error))
});

module.exports = router;
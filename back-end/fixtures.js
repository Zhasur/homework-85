const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const artist = await Artist.create(
        {
            title: 'Ceza',
            image: 'ceza.jpg',
            information: 'Bilgin Özçalkan (born 31 December 1976), also known by his stage name Ceza' +
                ' (pronounced je-ZAH, Turkish for \'punishment\'), is a Turkish rapper and songwriter.' +
                ' His 2015 song "Suspus" has been compared to Eminem\'s anti-Trump freestyle rap "Storm"'
        },
        {
            title: 'Taylor Swift',
            image: 'Taylor_Swift-reputation.jpg',
            information: 'Taylor Alison Swift (born December 13, 1989) is an American singer-songwriter. ' +
                'As one of the world\'s leading contemporary recording artists, she is known for narrative songs about' +
                ' her personal life, which has received widespread media coverage.'
        }
    );

    const album = await Album.create(
        {
            title: 'Suspus (Album)',
            artist: artist[0]._id,
            releaseDate: 2015,
            image: 'ceza.jpg'
        },
        {
            title: 'Rapstar (Album)',
            artist: artist[0]._id,
            releaseDate: 2004,
            image: 'holocaust.jpg'
        },
        {
            title: 'Reputation (Album)',
            artist: artist[1]._id,
            releaseDate: 2019,
            image: 'Taylor_Swift-reputation.jpg'
        }
    );


    await Track.create(
        {
            title: 'Suspus',
            albums: album[0]._id,
            duration: '4:21',
            trackNumber: 1,
            artist: artist[0].title
        },
        {
            title: 'Milion Farkli Hikaye',
            albums: album[0]._id,
            duration: '3:47',
            trackNumber: 2,
            artist: artist[0].title
        },
        {
            title: 'Ne de Zor',
            albums: album[0]._id,
            duration: '3:32',
            trackNumber: 3,
            artist: artist[0].title
        },{
            title: 'Ac Kalbini',
            albums: album[0]._id,
            duration: '3:45',
            trackNumber: 4,
            artist: artist[0].title
        },
        {
            title: 'Hosgeldiniz',
            albums: album[0]._id,
            duration: '2:06',
            trackNumber: 5,
            artist: artist[0].title
        },
        {
            title: 'Rapstar',
            albums: album[1]._id,
            duration: '3:20',
            trackNumber: 1,
            artist: artist[0].title
        },
        {
            title: 'Panorama Harem',
            albums: album[1]._id,
            duration: '3:17',
            trackNumber: 2,
            artist: artist[0].title
        },
        {
            title: 'Holocaust',
            albums: album[1]._id,
            duration: '3:28',
            trackNumber: 3,
            artist: artist[0].title
        },
        {
            title: 'Look What You Made Me Do)',
            albums: album[2]._id,
            duration: '4:15',
            trackNumber: 1,
            artist: artist[1].title
        },
        {
            title: '…Ready For It?',
            albums: album[2]._id,
            duration: '3:30',
            trackNumber: 2,
            artist: artist[1].title
        },
        {
            title: 'End Game',
            albums: album[2]._id,
            duration: '1:41',
            trackNumber: 3,
            artist: artist[1].title
        },
        {
            title: 'Don\'t Blame Me' ,
            albums: album[2]._id,
            duration: '3:56',
            trackNumber: 4,
            artist: artist[1].title
        },
        {
            title: 'So It Goes',
            albums: album[2]._id,
            duration: '3:48',
            trackNumber: 5,
            artist: artist[1].title
        },
    );

    await connection.close();

};

run().catch(error => {
    console.error('Something went wrong!', error);
});
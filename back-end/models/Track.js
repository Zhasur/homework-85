const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TrackSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    albums: {
        type: Schema.Types.ObjectId,
        ref: 'Album',
        required: true
    },
    duration: {
        type: String
    },
    trackNumber: {
        type: String
    },
    artist: String
});

const Track = mongoose.model('Track', TrackSchema);

module.exports = Track;
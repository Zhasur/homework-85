import axios from "../../axios-musics";

export const FETCH_ALBUM_SUCCESS = 'FETCH_ALBUM_SUCCESS';
export const FETCH_ALBUM_FAIL = 'FETCH_ALBUM_FAIL';

const fetchAlbumSuccess = (albums) => ({type: FETCH_ALBUM_SUCCESS, albums});
const fetchAlbumError = () => ({type: FETCH_ALBUM_FAIL});

export const fetchSuccess = (id) => {
    return dispatch => {
        return axios.get('/albums/' + id)
            .then(response => {
                dispatch(fetchAlbumSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchAlbumError(error))
            })
    }
};
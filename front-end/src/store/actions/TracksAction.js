import axios from '../../axios-musics'

export const FETCH_TRACKS_SUCCESS = "FETCH_TRACKS_SUCCESS";
export const FETCH_TRACKS_ERROR = "FETCH_TRACKS_ERROR";

const fetchTracksSuccess = (tracks) => ({type: FETCH_TRACKS_SUCCESS, tracks});
const fetchTracksError = () => ({type: FETCH_TRACKS_ERROR});

export const fetchSuccess = (id) => {
    return dispatch => {
        return axios.get('/tracks/' + id)
            .then(response => {
                dispatch(fetchTracksSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchTracksError(error))
            })
    }
};
